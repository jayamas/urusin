import React from 'react';
import { View, Text, TouchableOpacity, Dimensions} from 'react-native';
import {Thumbnail, Right, ListItem, List, Left,Body,Button} from 'native-base'
import {colors} from '../../utils/colors'

const dimension=Dimensions.get('window')
const HistoryComponent = (props) => {
    return(
        <List style={{ borderBottomWidth: 2, borderBottomColor: colors.Text_color, marginHorizontal: 15, }}>
            <ListItem thumbnail>
                <Left>
                    <View style={{
                        backgroundColor: colors.app_theme_color, 
                        borderRadius:7, 
                        width: dimension.width*0.20,
                        height: dimension.width*0.20
                        }}></View>
                </Left>
                <Body>
                    <Text style={{ fontSize: 14, fontWeight: '500', color: colors.Text_color }} >{props.dateOrder}</Text>
                    <Text style={{ fontSize: 16, fontWeight:'bold', color: colors.Text_color }}>{ props.orderType}</Text>
                    <Text style={{ fontSize: 12, fontWeight:'200', color: colors.Text_color }}> {props.company}</Text>
                </Body>
                <Right>
                    <View style={{ 
                                backgroundColor: colors.lemonYellow,
                                borderRadius:1000,
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                        <Text style={{ padding:20, fontSize:18, fontWeight:'bold', color:colors.Text_color }}>{props.RightText}</Text>
                    </View>
                </Right>
            </ListItem>
        </List>
        
    )
}
export default HistoryComponent;
