export const NavigationKeys = {
	splash: 'splash',
	home:'home',
	tab_view:'tab_view',
};

export const Strings = {
	app_name: 'GoWist',
	hello_guest: 'Hello, Guest',
	glad_your_holiday: 'Glad your holiday',
	flight: 'Flight',
	ordering_data: 'E-Tiket Booker Data',
	booking_detail: 'Booker Data',
	title: 'Title',
	first_name: 'Name Depan',
	last_name: 'Nama Belakang',
	phn_number: 'Nomor Handphone',
	email: 'Email',
	confirmation_text:
		'Kami akan mengirimkan konfirmasi pesanan Anda ka kontak di atas, yang juga akan digunakan untuk keperluan Refund atau Reschedule.',
	confirmation_text_eng:
		'We will send confirmation your code order via contact above, which will also needed for refund or resechedule',
	next: 'LANJUTKAN',
	data_booking: 'Data Booking',
	payment_method: 'Select Payment Method',
	booking_summary: 'Booking Summary',
	flight_detail: 'Flight Details',
	order_detail_e_booking: 'Detail Pemesan (untuk E-Tiket/Voucher)',
	passenger_detail: 'Detail Penumpang',
	passenger_data: 'E-Tiket Passenger Data',
	enter_passenger: 'Masukkan sebagai penumpang',
	born_date: 'Tanggal Lahir',
	born_date_place_holder: 'Pilih tanggal lahir',
	no_flights_matches: 'No flights match your preference',
	choose_baggage_capacity: 'Pilih kapasitas bagasi sesuai kebutuhan Anda.',
	passportnationality: 'Kewarganegaraan',
	passportNumber: 'Nomor Paspor',
	passportExpireDate: 'Tanggal Berakhin Paspor',
	passportIssuing: 'Negara Yang Mengeluarkan',
	coming_soon: 'Coming soon',
	passportExpiryDate: 'Tanggal kedaluwarsa paspor',
	gowist_caps: 'GOWIST',
	mbr_mobile: 'UNKNOWN',
	usrIdTrain: '0895805066010',
	deviceTrainAPI: '866645039470567',
	cost: 'Total Cost',
	header_confirm: 'Is Your Order Correct?',
	info_confirm: 'You cant change the order detile after your continue this steps, do you want to continue?'
};
export const VectorIconName = {
	drawer_icon: 'dots-three-vertical',
	bell_icon: 'bell-o',
	power_off: 'power-off',
	back_icon: 'chevron-thin-left',
	left_arrow: 'chevron-thin-left',
	right_arrow: 'chevron-thin-right',
	greetings: 'Glad your holiday',
	arrow_right: 'arrowright',
	login_icon: 'login',
	info: 'info',
	chevron_right: 'chevron-right',
	angle_right: 'angle-right',
	chevron_small_right: 'chevron-small-right',
	circle_with_cross: 'circle-with-cross',
	dots_three_vertical: 'dots-three-vertical'
};

export const Messages = {
	internetConnection: 'Please check your internet connection.',
	empty_phn: 'Phone number field is empty.',
	invalid_phnNo: 'Phone number is not valid.',
	empty_password: 'Password field is empty',
	invalid_password: 'Password is not valid.',
	login_successfully: 'Login successfully.',
	invalid_username_password: 'Invalid username or password.',
	something_went_wrong: 'Something went wrong.',
	no_data_Found: 'No Data Found.',
	select_departure: 'Please select departure date.',
	select_from: 'Please select your origin.',
	select_to: 'Please select your destination.',
	coming_soon: 'Coming soon.',
	logout_successfully: 'Logout successfully.'
};

export const AsyncStorageKeys = {
	accessToken: 'accessToken',
	tokenAcces: 'tokenAcces',
	tokenFlight: 'tokenFlight',
	tokenTrain: 'tokenTrain',
	tokenCourse: 'tokenCourse',
	user_data: 'user_data',
	user_id: 'user_id',
	user_name: 'user_name',
	email_id: 'email_id',
	saldo: 'saldo',
	bonus: 'bonus',
	imei: 'imei',

	//TRAIN
	id_station: 'dari',
	station_awal: 'dari_stasiun',
	id_station_tujuan: 'tujuan',
	station_tujuan: 'tujuan_statsion',
	tglBrangkat: 'tglBerangkat'
};

