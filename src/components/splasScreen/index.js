import React, {Component} from 'react';
import {
    Image,
    View,
    StyleSheet,
    Dimensions
} from 'react-native';
import {Actions, ActionConst} from "react-native-router-flux";
import {HomeImages} from ".././../utils/imagesPath";
import {colors} from '../../utils/colors'

type Props = {};
const dimensions = Dimensions.get('window')
export default class Splash extends Component<Props> {

    componentDidMount(): void {
        setTimeout(() => {
            Actions.HomePage({type: ActionConst.RESET})
        }, 1000 * 2)
    }

    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.logo}
                    source={HomeImages.SPLASH_LOGO}
                    resizeMode={'contain'}
                />
            </View>
        );
    }
}

const styles=StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:colors.backgorund_blue,
    },
    logo: {
        alignSelf: 'center',
        margin: 10,
        width: dimensions.width * 0.40,
        height: dimensions.width * 0.40,
    },
});