import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux'
import Header from '../../../components/header/index'
import Footer from '../../../components/footerTab/index'
import {colors} from '../../../utils/colors'

const dimension = Dimensions.get('window')
class Laundry extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex: 1,backgroundColor: colors.white_color, }}>
        <Header/>
        <View style={{ justifyContent: 'center', alignItems: 'center', padding:10 }}>
          <Text style={{ fontSize: 36, fontWeight: 'bold', color:colors.Text_color }}> Laundry </Text>
        </View>
        
        <View style={{ marginHorizontal: 15, justifyContent: 'center', alignItems: 'center', }}>
          <TouchableOpacity style={{ width:'100%', height:dimension.height*0.25, backgroundColor: colors.blue_air_color,borderRadius: 7, marginVertical: 5, }}>
          </TouchableOpacity>
          <Text style={{ fontSize: 16, fontWeight: '200', color:colors.blue_air_color }}> Premium Laundry </Text>

          <TouchableOpacity style={{ width:'100%', height:dimension.height*0.25, backgroundColor: colors.blue_air_color,borderRadius: 7, marginVertical: 5, }}>
          </TouchableOpacity>
          <Text style={{ fontSize: 16, fontWeight: '200', color:colors.blue_air_color }}> Regular Laundry </Text>

        </View>
        

      <Footer/>
      </View>
    );
  }
}

export default Laundry;
