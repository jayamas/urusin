import type {Node} from 'react'

type Props = {
    condition: boolean,
    children: Node | Function,
}

const renderChildren = children => (typeof children === 'function' ? children() : children)

const RenderIf = ({children, condition}: Props) => (condition ? renderChildren(children) : null)

export default RenderIf
