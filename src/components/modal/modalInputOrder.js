import React, { Component } from 'react';
import { View, Text,TextInput, Modal, Image, Dimensions, SafeAreaView, TouchableOpacity,} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import propTypes from "prop-types";
import {colors} from '../../utils/colors'
import { ICON_SOSMED, BottomMenu } from '../../utils/imagesPath';
import styles from './style';
import ModalComp from '../componentHistory/index'


const dimension = Dimensions.get('window');
class modalInputOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
        visible: false,
        value :'',
    };
  }
    static propTypes = {
        visibleModal: propTypes.any,
        value: propTypes.any,
        data: propTypes.any,
        onClick: propTypes.func,
        closeModal: propTypes.func,
    };

    componentDidMount(){
        this.setState({visible: this.props.visibleModal});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            visible: nextProps.visibleModal,
            value: nextProps.value,
            data: nextProps.data
        });
    }

  render() {
    return (
        <Modal backdropColor={'green'}
                backdropOpacity={1}
                visible={this.state.visible}
                animationType={'slide'}
                transparent={true}>

            <SafeAreaView style ={styles.mainContainer}>
                <View style={styles.container}>
                    <View style={{paddingHorizontal: 20}}>
                       <TouchableOpacity 
                            onPress={()=>this.setState({visible: false})}
                            style={{flexDirection: 'column', alignSelf: 'center',justifyContent: 'center',alignItems: 'center',}}>
                            <Image
                                source={ICON_SOSMED.ICON_MODAL} 
                                style={{ width: 85, height:25 }}
                                resizeMode={'stretch'}    
                            /> 
                            <Text style={styles.titleText}>{'Order Status'}</Text>
                        </TouchableOpacity>
                    </View>  
                    <ModalComp 
                        dateOrder={'05 Oct 2019'}
                        orderType={'Regular Laundry'}
                        company={'Ayo Laundry'}
                        RightText={'4.7'}
                    />
                    <View style={{ marginHorizontal:45, flexDirection: 'row',alignItems: 'center', justifyContent:'space-between', marginVertical:10}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Cuci Basah</Text>
                        <View style={{ 
                            justifyContent: 'space-between', 
                            flexDirection:'row',
                            alignItems: 'center', 
                            height:35, 
                            width:dimension.width * 0.40, 
                            borderRadius: 30, 
                            borderColor: colors.white_color, 
                            borderWidth: 0.5, 
                            backgroundColor:colors.transparent,
                             }}>
                            <TouchableOpacity>
                                <Image source={BottomMenu.MINUS} style={{width:10, height:20, marginHorizontal:5}}/>
                            </TouchableOpacity>
                            <TextInput style={{ fontWeight: '100', backgroundColor:colors.white_color, flex: 1, }}></TextInput>
                            <TouchableOpacity>
                                <Image source={BottomMenu.PLUS} style={{width:10, height:10, marginHorizontal:5}}/>
                            </TouchableOpacity>
                            
                        </View>
                    </View>
                    <View style={{ 
                            marginHorizontal:45, 
                            flexDirection: 'row',
                            alignItems: 'center', 
                            justifyContent:'space-between', 
                            marginVertical:10
                        }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Cuci Kering</Text>
                        <View style={{ 
                                justifyContent: 'space-between', 
                                alignItems: 'center', 
                                height:35, 
                                width:dimension.width * 0.40, 
                                borderRadius: 30, 
                                borderColor: colors.white_color, 
                                borderWidth: 0.5, 
                                flexDirection:'row',
                                backgroundColor:colors.transparent 
                            }}>
                            <TouchableOpacity>
                                <Image source={BottomMenu.MINUS} style={{width:10, height:20, marginHorizontal:5, }}/>
                            </TouchableOpacity>
                            <TextInput style={{ fontWeight: '100', backgroundColor:colors.white_color, flex: 1, }}></TextInput>
                            <TouchableOpacity>
                                <Image source={BottomMenu.PLUS} style={{width:10, height:10, marginHorizontal:5}}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ 
                            marginHorizontal:45, 
                            flexDirection: 'row',
                            alignItems: 'center', 
                            justifyContent:'space-between', 
                            marginVertical:10
                        }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color:colors.white_color }}>Cuci Setrika</Text>
                        <View style={{ 
                            justifyContent: 'space-between', 
                            flexDirection:'row',
                            alignItems: 'center', 
                            height:40, 
                            width:dimension.width * 0.40, 
                            borderRadius: 30, 
                            borderColor: colors.white_color, 
                            borderWidth: 0.5, 
                            backgroundColor:colors.transparent }}>
                            <TouchableOpacity>
                                <Image source={BottomMenu.MINUS} style={{width:10, height:20, marginHorizontal:5}}/>
                            </TouchableOpacity>
                            <TextInput style={{ fontWeight: '100', backgroundColor:colors.white_color, flex: 1, }}></TextInput>
                            <TouchableOpacity>
                                <Image source={BottomMenu.PLUS} style={{width:10, height:10, marginHorizontal:5}}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={{flexDirection:'row', backgroundColor: colors.lemonYellow, justifyContent:'space-between', alignItems: 'center', borderRadius:30, margin:15, height:50}}>
                        <Text style={{ fontSize:16, fontWeight:'bold', color:colors.white_color, paddingLeft: 10,  }}>Order Regular Laundry </Text>
                        <Image source={BottomMenu.ARROWDOUBLE_RIGHT} style={{ width:35, height:35, paddingRight: 10, marginRight:15 }} resizeMode={'contain'}/>
                    </TouchableOpacity>
                </View>
                
            </SafeAreaView>
        </Modal>
    );
  }
}

export default modalInputOrder;
