import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, TextInput} from 'react-native';
import {Actions} from 'react-native-router-flux'
import { HomeImages, ICON_SOSMED} from '../../../utils/imagesPath';
import { colors } from '../../../utils/colors';
import TexInputComp from '../../../components/common/TextInput'
const dimension=Dimensions.get('window')
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.backgorund_blue, }}>
        <View style={{ alignItems: 'center',justifyContent: 'center', marginHorizontal: dimension.height*0.20, marginVertical:dimension.height*0.10, }}>
          <Image source={HomeImages.LOGO_LOGIN} style={{ width:dimension.width*0.45, height:dimension.width *0.45 }} resizeMode={'contain'}/>
        </View>
        <View style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                borderColor: colors.white_color, 
                borderWidth: 1, 
                alignItems: 'center', 
                borderRadius: 30,
                marginHorizontal:45
              }}>
          <TextInput placeholder='Username/Email'/>
        </View>
        <View style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                borderColor: colors.white_color, 
                borderWidth: 1, 
                alignItems: 'center', 
                borderRadius: 30,
                marginVertical: 15,
                marginHorizontal:45
              }}>
          <TextInput placeholder='Password'/>
        </View>
        <TouchableOpacity 
              onPress={()=> Actions.HomePage()}
              style={{ 
                justifyContent: 'center', 
                flexDirection: 'row', 
                backgroundColor: colors.white_color, 
                alignItems: 'center', 
                borderRadius: 30,
                marginVertical: 15,
                marginHorizontal:45
              }}>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color:colors.Text_color, paddingVertical: 13, }}>SIGN IN</Text>
        </TouchableOpacity>
        <View style={{ justifyContent: 'center',alignItems: 'center', }}>
          <Text> OR </Text>
          <Text> login with </Text>
        </View>

        <View style={{marginVertical:10,flexDirection: 'row', justifyContent: 'center',alignItems: 'center', }}>
          <Image  source={ICON_SOSMED.GOOGLE}  style={{ width:25, height:25 }} resizeMode={'contain'}/>
          <Image source={ICON_SOSMED.FB}  style={{ width:35, height:35 }} resizeMode={'contain'}/>
          <Image source={ICON_SOSMED.TWITTER}  style={{ width:35, height:35 }} resizeMode={'contain'}/>
        </View>
        <TouchableOpacity 
              onPress={()=> Actions.Register()}
              style={{ 
                justifyContent: 'center',
                alignItems: 'center',
                bottom:0
              }}>
          <Text>SIGN UP</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Login;
