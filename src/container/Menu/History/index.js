import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, ScrollView, FlatList} from 'react-native';
import {colors} from '../../../utils/colors';
import {Actions} from 'react-native-router-flux';
import ToolBar from '../../../components/toolBar/toolbarHome';
import Footer from '../../../components/footerTab/index';
import HistoryComponent from '../../../components/componentHistory/index'

const dimension=Dimensions.get('window')
class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex:1, backgroundColor: colors.white_color, }}>
        <ToolBar isDrawer={false} titleName={'History'}/>
        <ScrollView>
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
            <HistoryComponent 
                dateOrder={'05 Oct 2019'}
                orderType={'Regular Laundry'}
                company={'Ayo Laundry'}
                RightText={'4.7'}
            />
        </ScrollView>
        <View style={{ marginBottom: 20, padding:20 }}/>
        <Footer/>
      </View>
    );
  }
}

export default History;
