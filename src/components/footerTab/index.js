import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux'
import { HomeImages, Extras, BottomMenu } from '../../utils/imagesPath'
import { colors } from '../../utils/colors'
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ 
            backgroundColor: colors.white_color, 
            flexDirection: 'row', 
            alignItems: 'center', 
            justifyContent: 'space-between', 
            position: 'absolute', 
            bottom:0, 
            backgroundColor: colors.white_color , 
            flex:1,
            width:'100%',
            padding:8
          }}>
        <TouchableOpacity onPress={()=> Actions.jump()}>
            <Image source={HomeImages.HOME} style={{ width: 45, height:45, marginHorizontal: 15,}} resizeMode={'contain'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> Actions.Order()}>
            <Image source={HomeImages.MENU_ICON} style={{ width: 45, height:45, marginHorizontal: 15,}} resizeMode={'contain'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> Actions.History()}>
            <Image source={HomeImages.MAIL} style={{ width: 45, height:45, marginHorizontal: 15,}} resizeMode={'contain'}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=> Actions.Profile()}>
            <Image source={HomeImages.USER} style={{ width: 45, height:45, marginHorizontal: 15,}} resizeMode={'contain'}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Footer;
