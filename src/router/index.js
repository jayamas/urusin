// Routes for all classes are defined here
import React, { Component } from 'react';
import { Scene, Router, Drawer } from 'react-native-router-flux';
import { View, StatusBar, Dimensions, Alert } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import { colors } from '../utils/colors';
import Splash from '../components/splasScreen/index'
import HomePage from '../container/Home/index';
import Laundry from '../container/Menu/Laundry/index';
import CarWash from '../container/Menu/CarWash';
import Notifikasi from '../container/Menu/Notifikasi/index';
import Order from '../container/Menu/Order/index';
import Profile from '../container/Menu/Profile/index';
import Mitra from '../container/Menu/Mitra/index';
import History from '../container/Menu/History/index'
import Login from '../container/Auth/Login/index';
import Register from '../container/Auth/Register/index';


const windows = Dimensions.get('window');
export default class AppRouter extends Component {
	constructor(props) {
		super(props);
	}
	// componentDidMount(): void {
    //     setTimeout(() => {
    //         SplashScreen.hide();
    //     }, 1000)
    // }
	render() {
		return (
			<View style={{ flex: 1, backgroundColor:colors.transparent }} headerMode={false}>
				<StatusBar barStyle="light-content" backgroundColor={colors.app_theme_color} />
                    <Router>
                        <Scene key="root">
							<Scene key={'splash'}
                                   drawerLockMode={'locked-closed'}
                                   component={Splash}
                                   panHandlers={null} hideNavBar
                                   hideDrawerButton initial={true}/>
                            <Scene key={'HomePage'} component={HomePage} initial={false} hideNavBar={true}/>
                            <Scene key={'Laundry'} component={Laundry} hideNavBar={true} />
                            <Scene key={'CarWash'} component={CarWash} hideNavBar={true}/>
                            <Scene key={'Notifikasi'} component={Notifikasi} hideNavBar={true}/>
                            <Scene key={'Order'} component={Order} hideNavBar={true} initial={false}/>
							<Scene key={'Profile'} component={Profile} hideNavBar={true}/>
							<Scene key={'Mitra'} component={Mitra} hideNavBar={true}/>
							<Scene key={'History'} component={History} hideNavBar={true}/>
							<Scene key={'Login'} component={Login} hideNavBar={true} initial={false}/>
							<Scene key={'Register'} component={Register} hideNavBar={true} />
                        </Scene>
                    </Router>
			</View>
		);
	}
}
