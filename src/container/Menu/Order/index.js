import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux'

import { colors } from '../../../utils/colors';
import Footer from '../../../components/footerTab/index';
import ToolBar from '../../../components/toolBar/toolbarHome';
import OrderStatusModal from '../../../components/modal/modalOrderStatus'

const dimension=Dimensions.get('window')
class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      StatusOrderModal: false,
      order_value:'',
    };
  }
  triggerStatusOrderModal() {
    this.setState({
      StatusOrderModal: true,
    });
  }
  closeSortByModal() {
      this.setState({
          StatusOrderModal: false
      })
  }

  render() {
    const {StatusOrderModal} = this.state;
    return (
      <View style={{ flex:1, backgroundColor: colors.white_color, }}>
        <ToolBar isDrawer={false} titleName={' ongoing Order'}/>

        <View style={{ marginHorizontal: 15, flexDirection: 'row', }}>
          <View style={{margin:10, width:dimension.width * 0.20, height: dimension.width *0.20, backgroundColor: colors.Text_color, borderRadius:7 }}>
          </View>
          <View style={{ marginVertical: 10, }}>
            <Text style={{ fontSize: 16, fontWeight: '500', color:colors.blue_color }}> {'24 Oct 2019'}</Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color:colors.blue_color }}> Regular Laundry</Text>
            <Text style={{ fontSize: 14, fontWeight: '200', color:colors.blue_color }}> Mama Laundry </Text>
          </View>
        </View>
        <TouchableOpacity 
            onPress={() => this.setState({ StatusOrderModal: true })}
            style={{ backgroundColor: colors.lemonYellow, marginHorizontal:15, borderRadius:20, justifyContent: 'center', alignItems:'center', }}>
          <Text style={{ fontSize:16, fontWeight:'bold', color:colors.black, padding:10 }}>Menunggu Konfirmasi</Text>
        </TouchableOpacity>

        <View style={{ marginHorizontal: 15, flexDirection: 'row', }}>
          <View style={{margin:10, width:dimension.width * 0.20, height: dimension.width *0.20, backgroundColor: colors.Text_color, borderRadius:7 }}>
          </View>
          <View style={{ marginVertical: 10, }}>
            <Text style={{ fontSize: 16, fontWeight: '500', color:colors.blue_color }}> {'20 Oct 2019'}</Text>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color:colors.blue_color }}> Regular Laundry</Text>
            <Text style={{ fontSize: 14, fontWeight: '200', color:colors.blue_color }}> Ayo Laundry </Text>
          </View>
        </View>
        <TouchableOpacity style={{ backgroundColor: colors.lemonYellow, marginHorizontal:15, borderRadius:20, justifyContent: 'center', alignItems:'center', }}>
          <Text style={{ fontSize:16, fontWeight:'bold', color:colors.black, padding:10 }}>Di Kirim</Text>
        </TouchableOpacity>
        <Footer/>
         <OrderStatusModal
            visibleModal={this.state.StatusOrderModal}
            value={''}
            onClick={(param) => { alert(param), this.setState({ StatusOrderModal: false }) }} />
      </View>
    );
  }
}

export default Order;
